#!/bin/bash
#
# this is the script run by the Jenkins and gitlab-ci continuous integration
# build services.  It is a thorough set of tests that runs all the tests using
# the various methods of installing/running fdroidserver.  It is separate from
# ./tests/run-tests because its too heavy for manual use.

if [ `dirname $0` != "." ]; then
    echo "only run this script like ./`basename $0`"
    exit 1
fi

set -e
set -x

if [ -z $WORKSPACE ]; then
    WORKSPACE=`cd $(dirname $0)/.. && pwd`
    echo "Setting Workspace to $WORKSPACE"
fi

if [ -z $ANDROID_HOME ]; then
    if [ -e ~/.android/bashrc ]; then
        . ~/.android/bashrc
    else
        echo "ANDROID_HOME must be set!"
        exit 1
    fi
fi

if ! which pyvenv; then
    echo "pyvenv required to run this test suite!"
    exit 1
fi

apksource=$1

#------------------------------------------------------------------------------#
# cache pypi downloads
if [ -z $PIP_DOWNLOAD_CACHE ]; then
    export PIP_DOWNLOAD_CACHE=$HOME/.pip_download_cache
fi

#------------------------------------------------------------------------------#
# run local tests, don't scan fdroidserver/ project for APKs

cd $WORKSPACE/tests
./run-tests $apksource


#------------------------------------------------------------------------------#
# test install using install direct from git repo
cd $WORKSPACE
rm -rf $WORKSPACE/env
pyvenv $WORKSPACE/env
. $WORKSPACE/env/bin/activate
pip3 install --quiet -e $WORKSPACE
python3 setup.py install

# run tests in new pip+pyvenv install
fdroid=$WORKSPACE/env/bin/fdroid $WORKSPACE/tests/run-tests $apksource

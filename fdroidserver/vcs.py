#!/usr/bin/env python3
#
# vcs.py - part of the FDroid server tools
# Copyright (C) 2010-13, Ciaran Gultnieks, ciaran@ciarang.com
# Copyright (C) 2013-2014 Daniel Martí <mvdan@mvdan.cc>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import re
import shutil

from fdroidserver import _, common
from fdroidserver.exception import FDroidException, NoSubmodulesException, VCSException


class vcs:
    def __init__(self, remote, local):
        # svn, git-svn and bzr may require auth
        self.username = None
        if self.repotype() in ("git-svn", "bzr"):
            if "@" in remote:
                if self.repotype == "git-svn":
                    raise VCSException("Authentication is not supported for git-svn")
                self.username, remote = remote.split("@")
                if ":" not in self.username:
                    raise VCSException(_("Password required with username"))
                self.username, self.password = self.username.split(":")

        self.remote = remote
        self.local = local
        self.clone_failed = False
        self.refreshed = False
        self.srclib = None

    def repotype(self):
        return None

    def clientversion(self):
        versionstr = common.FDroidPopen(self.clientversioncmd()).output
        return versionstr[0 : versionstr.find("\n")]

    def clientversioncmd(self):
        return None

    def gotorevision(self, rev, refresh=True):
        """Take the local repository to a clean version of the given
        revision, which is specificed in the VCS's native
        format. Beforehand, the repository can be dirty, or even
        non-existent. If the repository does already exist locally, it
        will be updated from the origin, but only once in the lifetime
        of the vcs object.  None is acceptable for 'rev' if you know
        you are cloning a clean copy of the repo - otherwise it must
        specify a valid revision.
        """

        if self.clone_failed:
            raise VCSException(
                _("Downloading the repository already failed once, not trying again.")
            )

        # The .fdroidvcs-id file for a repo tells us what VCS type
        # and remote that directory was created from, allowing us to drop it
        # automatically if either of those things changes.
        fdpath = os.path.join(self.local, "..", ".fdroidvcs-" + os.path.basename(self.local))
        fdpath = os.path.normpath(fdpath)
        cdata = self.repotype() + " " + self.remote
        writeback = True
        deleterepo = False
        if os.path.exists(self.local):
            if os.path.exists(fdpath):
                with open(fdpath, "r") as f:
                    fsdata = f.read().strip()
                if fsdata == cdata:
                    writeback = False
                else:
                    deleterepo = True
                    logging.info("Repository details for %s changed - deleting" % (self.local))
            else:
                deleterepo = True
                logging.info("Repository details for %s missing - deleting" % (self.local))
        if deleterepo:
            shutil.rmtree(self.local)

        exc = None
        if not refresh:
            self.refreshed = True

        try:
            self.gotorevisionx(rev)
        except FDroidException as e:
            exc = e

        # If necessary, write the .fdroidvcs file.
        if writeback and not self.clone_failed:
            os.makedirs(os.path.dirname(fdpath), exist_ok=True)
            with open(fdpath, "w+") as f:
                f.write(cdata)

        if exc is not None:
            raise exc

    def gotorevisionx(self, rev):  # pylint: disable=unused-argument
        """Derived classes need to implement this.

        It's called once basic checking has been performed.
        """
        raise VCSException("This VCS type doesn't define gotorevisionx")

    # Initialise and update submodules
    def initsubmodules(self):
        raise VCSException("Submodules not supported for this vcs type")

    # Get a list of all known tags
    def gettags(self):
        if not self._gettags:
            raise VCSException("gettags not supported for this vcs type")
        rtags = []
        for tag in self._gettags():
            if re.match("[-A-Za-z0-9_. /]+$", tag):
                rtags.append(tag)
        return rtags

    def latesttags(self):
        """Get a list of all the known tags, sorted from newest to oldest"""
        raise VCSException("latesttags not supported for this vcs type")

    def getref(self):
        """Get current commit reference (hash, revision, etc)"""
        raise VCSException("getref not supported for this vcs type")

    def getsrclib(self):
        """Returns the srclib (name, path) used in setting up the current revision, or None."""
        return self.srclib


class vcs_git(vcs):
    def repotype(self):
        return "git"

    def clientversioncmd(self):
        return ["git", "--version"]

    def git(self, args, envs=dict(), cwd=None, output=True):
        """Prevent git fetch/clone/submodule from hanging at the username/password prompt

        While fetch/pull/clone respect the command line option flags,
        it seems that submodule commands do not.  They do seem to
        follow whatever is in env vars, if the version of git is new
        enough.  So we just throw the kitchen sink at it to see what
        sticks.

        Also, because of CVE-2017-1000117, block all SSH URLs.
        """
        #
        # supported in git >= 2.3
        git_config = [
            "-c",
            "core.askpass=/bin/true",
            "-c",
            "core.sshCommand=/bin/false",
            "-c",
            "url.https://.insteadOf=ssh://",
        ]
        for domain in ("bitbucket.org", "github.com", "gitlab.com"):
            git_config.append("-c")
            git_config.append("url.https://u:p@" + domain + "/.insteadOf=git@" + domain + ":")
            git_config.append("-c")
            git_config.append("url.https://u:p@" + domain + ".insteadOf=git://" + domain)
            git_config.append("-c")
            git_config.append("url.https://u:p@" + domain + ".insteadOf=https://" + domain)
        envs.update(
            {
                "GIT_TERMINAL_PROMPT": "0",
                "GIT_ASKPASS": "/bin/true",
                "SSH_ASKPASS": "/bin/true",
                "GIT_SSH": "/bin/false",  # for git < 2.3
            }
        )
        return common.FDroidPopen(
            [
                "git",
            ]
            + git_config
            + args,
            envs=envs,
            cwd=cwd,
            output=output,
        )

    def checkrepo(self):
        """If the local directory exists, but is somehow not a git repository,
        git will traverse up the directory tree until it finds one
        that is (i.e.  fdroidserver) and then we'll proceed to destroy
        it!  This is called as a safety check.

        """

        p = common.FDroidPopen(
            ["git", "rev-parse", "--show-toplevel"], cwd=self.local, output=False
        )
        result = p.output.rstrip()
        if not result.endswith(self.local):
            raise VCSException("Repository mismatch")

    def gotorevisionx(self, rev):
        if not os.path.exists(self.local):
            # Brand new checkout
            p = self.git(["clone", "--", self.remote, self.local])
            if p.returncode != 0:
                self.clone_failed = True
                raise VCSException("Git clone failed", p.output)
            self.checkrepo()
        else:
            self.checkrepo()
            # Discard any working tree changes
            p = common.FDroidPopen(
                [
                    "git",
                    "submodule",
                    "foreach",
                    "--recursive",
                    "git",
                    "reset",
                    "--hard",
                ],
                cwd=self.local,
                output=False,
            )
            if p.returncode != 0:
                raise VCSException(_("Git reset failed"), p.output)
            # Remove untracked files now, in case they're tracked in the target
            # revision (it happens!)
            p = common.FDroidPopen(
                ["git", "submodule", "foreach", "--recursive", "git", "clean", "-dffx"],
                cwd=self.local,
                output=False,
            )
            if p.returncode != 0:
                raise VCSException(_("Git clean failed"), p.output)
            if not self.refreshed:
                # Get latest commits and tags from remote
                p = self.git(["fetch", "origin"], cwd=self.local)
                if p.returncode != 0:
                    raise VCSException(_("Git fetch failed"), p.output)
                p = self.git(
                    ["fetch", "--prune", "--tags", "--force", "origin"],
                    output=False,
                    cwd=self.local,
                )
                if p.returncode != 0:
                    raise VCSException(_("Git fetch failed"), p.output)
                # Recreate origin/HEAD as git clone would do it, in case it disappeared
                p = common.FDroidPopen(
                    ["git", "remote", "set-head", "origin", "--auto"],
                    cwd=self.local,
                    output=False,
                )
                if p.returncode != 0:
                    lines = p.output.splitlines()
                    if "Multiple remote HEAD branches" not in lines[0]:
                        logging.warning(_('Git remote set-head failed: "%s"') % p.output.strip())
                    else:
                        branch = lines[1].split(" ")[-1]
                        p2 = common.FDroidPopen(
                            ["git", "remote", "set-head", "origin", "--", branch],
                            cwd=self.local,
                            output=False,
                        )
                        if p2.returncode != 0:
                            logging.warning(
                                _('Git remote set-head failed: "%s"') % p.output.strip()
                                + "\n"
                                + p2.output.strip()
                            )
                self.refreshed = True
        # origin/HEAD is the HEAD of the remote, e.g. the "default branch" on
        # a github repo. Most of the time this is the same as origin/master.
        rev = rev or "origin/HEAD"
        p = common.FDroidPopen(["git", "checkout", "-f", rev], cwd=self.local, output=False)
        if p.returncode != 0:
            raise VCSException(_("Git checkout of '%s' failed") % rev, p.output)
        # Get rid of any uncontrolled files left behind
        p = common.FDroidPopen(["git", "clean", "-dffx"], cwd=self.local, output=False)
        if p.returncode != 0:
            raise VCSException(_("Git clean failed"), p.output)

    def initsubmodules(self):
        self.checkrepo()
        submfile = os.path.join(self.local, ".gitmodules")
        if not os.path.isfile(submfile):
            raise NoSubmodulesException(_("No git submodules available"))

        # fix submodules not accessible without an account and public key auth
        with open(submfile, "r") as f:
            lines = f.readlines()
        with open(submfile, "w") as f:
            for line in lines:
                for domain in ("bitbucket.org", "github.com", "gitlab.com"):
                    line = re.sub("git@" + domain + ":", "https://u:p@" + domain + "/", line)
                f.write(line)

        p = common.FDroidPopen(["git", "submodule", "sync"], cwd=self.local, output=False)
        if p.returncode != 0:
            raise VCSException(_("Git submodule sync failed"), p.output)
        p = self.git(["submodule", "update", "--init", "--force", "--recursive"], cwd=self.local)
        if p.returncode != 0:
            raise VCSException(_("Git submodule update failed"), p.output)

    def _gettags(self):
        self.checkrepo()
        p = common.FDroidPopen(["git", "tag"], cwd=self.local, output=False)
        return p.output.splitlines()

    tag_format = re.compile(r"tag: ([^),]*)")

    def latesttags(self):
        self.checkrepo()
        p = common.FDroidPopen(
            ["git", "log", "--tags", "--simplify-by-decoration", "--pretty=format:%d"],
            cwd=self.local,
            output=False,
        )
        tags = []
        for line in p.output.splitlines():
            for tag in self.tag_format.findall(line):
                tags.append(tag)
        return tags


class vcs_gitsvn(vcs):
    def repotype(self):
        return "git-svn"

    def clientversioncmd(self):
        return ["git", "svn", "--version"]

    def checkrepo(self):
        """If the local directory exists, but is somehow not a git repository,
        git will traverse up the directory tree until it finds one that
        is (i.e.  fdroidserver) and then we'll proceed to destory it!
        This is called as a safety check.

        """
        p = common.FDroidPopen(
            ["git", "rev-parse", "--show-toplevel"], cwd=self.local, output=False
        )
        result = p.output.rstrip()
        if not result.endswith(self.local):
            raise VCSException("Repository mismatch")

    def git(self, args, envs=dict(), cwd=None, output=True):
        """Prevent git fetch/clone/submodule from hanging at the username/password prompt

        AskPass is set to /bin/true to let the process try to connect
        without a username/password.

        The SSH command is set to /bin/false to block all SSH URLs
        (supported in git >= 2.3).  This protects against
        CVE-2017-1000117.

        """
        git_config = [
            "-c",
            "core.askpass=/bin/true",
            "-c",
            "core.sshCommand=/bin/false",
        ]
        envs.update(
            {
                "GIT_TERMINAL_PROMPT": "0",
                "GIT_ASKPASS": "/bin/true",
                "SSH_ASKPASS": "/bin/true",
                "GIT_SSH": "/bin/false",  # for git < 2.3
                "SVN_SSH": "/bin/false",
            }
        )
        return common.FDroidPopen(
            [
                "git",
            ]
            + git_config
            + args,
            envs=envs,
            cwd=cwd,
            output=output,
        )

    def gotorevisionx(self, rev):
        if not os.path.exists(self.local):
            # Brand new checkout
            gitsvn_args = ["svn", "clone"]
            remote = None
            if ";" in self.remote:
                remote_split = self.remote.split(";")
                for i in remote_split[1:]:
                    if i.startswith("trunk="):
                        gitsvn_args.extend(["-T", i[6:]])
                    elif i.startswith("tags="):
                        gitsvn_args.extend(["-t", i[5:]])
                    elif i.startswith("branches="):
                        gitsvn_args.extend(["-b", i[9:]])
                remote = remote_split[0]
            else:
                remote = self.remote

            if not remote.startswith("https://"):
                raise VCSException(_("HTTPS must be used with Subversion URLs!"))

            # git-svn sucks at certificate validation, this throws useful errors:
            try:
                import requests

                r = requests.head(remote)
                r.raise_for_status()
            except Exception as e:
                raise VCSException("SVN certificate pre-validation failed: " + str(e))
            location = r.headers.get("location")
            if location and not location.startswith("https://"):
                raise VCSException(
                    _("Invalid redirect to non-HTTPS: {before} -> {after} ").format(
                        before=remote, after=location
                    )
                )

            gitsvn_args.extend(["--", remote, self.local])
            p = self.git(gitsvn_args)
            if p.returncode != 0:
                self.clone_failed = True
                raise VCSException(_("git svn clone failed"), p.output)
            self.checkrepo()
        else:
            self.checkrepo()
            # Discard any working tree changes
            p = self.git(["reset", "--hard"], cwd=self.local, output=False)
            if p.returncode != 0:
                raise VCSException("Git reset failed", p.output)
            # Remove untracked files now, in case they're tracked in the target
            # revision (it happens!)
            p = self.git(["clean", "-dffx"], cwd=self.local, output=False)
            if p.returncode != 0:
                raise VCSException("Git clean failed", p.output)
            if not self.refreshed:
                # Get new commits, branches and tags from repo
                p = self.git(["svn", "fetch"], cwd=self.local, output=False)
                if p.returncode != 0:
                    raise VCSException("Git svn fetch failed")
                p = self.git(["svn", "rebase"], cwd=self.local, output=False)
                if p.returncode != 0:
                    raise VCSException("Git svn rebase failed", p.output)
                self.refreshed = True

        rev = rev or "master"
        if rev:
            nospaces_rev = rev.replace(" ", "%20")
            # Try finding a svn tag
            for treeish in ["origin/", ""]:
                p = self.git(
                    ["checkout", treeish + "tags/" + nospaces_rev],
                    cwd=self.local,
                    output=False,
                )
                if p.returncode == 0:
                    break
            if p.returncode != 0:
                # No tag found, normal svn rev translation
                # Translate svn rev into git format
                rev_split = rev.split("/")

                p = None
                for treeish in ["origin/", ""]:
                    if len(rev_split) > 1:
                        treeish += rev_split[0]
                        svn_rev = rev_split[1]

                    else:
                        # if no branch is specified, then assume trunk (i.e. 'master' branch):
                        treeish += "master"
                        svn_rev = rev

                    svn_rev = svn_rev if svn_rev[0] == "r" else "r" + svn_rev

                    p = self.git(
                        ["svn", "find-rev", "--before", svn_rev, treeish],
                        cwd=self.local,
                        output=False,
                    )
                    git_rev = p.output.rstrip()

                    if p.returncode == 0 and git_rev:
                        break

                if p.returncode != 0 or not git_rev:
                    # Try a plain git checkout as a last resort
                    p = self.git(["checkout", rev], cwd=self.local, output=False)
                    if p.returncode != 0:
                        raise VCSException(
                            "No git treeish found and direct git checkout of '%s' failed" % rev,
                            p.output,
                        )
                else:
                    # Check out the git rev equivalent to the svn rev
                    p = self.git(["checkout", git_rev], cwd=self.local, output=False)
                    if p.returncode != 0:
                        raise VCSException(_("Git checkout of '%s' failed") % rev, p.output)

        # Get rid of any uncontrolled files left behind
        p = self.git(["clean", "-dffx"], cwd=self.local, output=False)
        if p.returncode != 0:
            raise VCSException(_("Git clean failed"), p.output)

    def _gettags(self):
        self.checkrepo()
        for treeish in ["origin/", ""]:
            d = os.path.join(self.local, ".git", "svn", "refs", "remotes", treeish, "tags")
            if os.path.isdir(d):
                return os.listdir(d)

    def getref(self):
        self.checkrepo()
        p = common.FDroidPopen(["git", "svn", "find-rev", "HEAD"], cwd=self.local, output=False)
        if p.returncode != 0:
            return None
        return p.output.strip()


class vcs_hg(vcs):
    def repotype(self):
        return "hg"

    def clientversioncmd(self):
        return ["hg", "--version"]

    def gotorevisionx(self, rev):
        if not os.path.exists(self.local):
            p = common.FDroidPopen(
                ["hg", "clone", "--ssh", "/bin/false", "--", self.remote, self.local],
                output=False,
            )
            if p.returncode != 0:
                self.clone_failed = True
                raise VCSException("Hg clone failed", p.output)
        else:
            p = common.FDroidPopen(["hg", "status", "-uS"], cwd=self.local, output=False)
            if p.returncode != 0:
                raise VCSException("Hg status failed", p.output)
            for line in p.output.splitlines():
                if not line.startswith("? "):
                    raise VCSException("Unexpected output from hg status -uS: " + line)
                common.FDroidPopen(["rm", "-rf", "--", line[2:]], cwd=self.local, output=False)
            if not self.refreshed:
                p = common.FDroidPopen(
                    ["hg", "pull", "--ssh", "/bin/false"], cwd=self.local, output=False
                )
                if p.returncode != 0:
                    raise VCSException("Hg pull failed", p.output)
                self.refreshed = True

        rev = rev or "default"
        if not rev:
            return
        p = common.FDroidPopen(["hg", "update", "-C", "--", rev], cwd=self.local, output=False)
        if p.returncode != 0:
            raise VCSException("Hg checkout of '%s' failed" % rev, p.output)
        p = common.FDroidPopen(["hg", "purge", "--all"], cwd=self.local, output=False)
        # Also delete untracked files, we have to enable purge extension for that:
        if "'purge' is provided by the following extension" in p.output:
            with open(os.path.join(self.local, ".hg", "hgrc"), "a") as myfile:
                myfile.write("\n[extensions]\nhgext.purge=\n")
            p = common.FDroidPopen(["hg", "purge", "--all"], cwd=self.local, output=False)
            if p.returncode != 0:
                raise VCSException("HG purge failed", p.output)
        elif p.returncode != 0:
            raise VCSException("HG purge failed", p.output)

    def _gettags(self):
        p = common.FDroidPopen(["hg", "tags", "-q"], cwd=self.local, output=False)
        return p.output.splitlines()[1:]


class vcs_bzr(vcs):
    def repotype(self):
        return "bzr"

    def clientversioncmd(self):
        return ["bzr", "--version"]

    def bzr(self, args, envs=dict(), cwd=None, output=True):
        """Prevent bzr from ever using SSH to avoid security vulns"""
        envs.update(
            {
                "BZR_SSH": "false",
            }
        )
        return common.FDroidPopen(
            [
                "bzr",
            ]
            + args,
            envs=envs,
            cwd=cwd,
            output=output,
        )

    def gotorevisionx(self, rev):
        if not os.path.exists(self.local):
            p = self.bzr(["branch", self.remote, self.local], output=False)
            if p.returncode != 0:
                self.clone_failed = True
                raise VCSException("Bzr branch failed", p.output)
        else:
            p = self.bzr(
                ["clean-tree", "--force", "--unknown", "--ignored"],
                cwd=self.local,
                output=False,
            )
            if p.returncode != 0:
                raise VCSException("Bzr revert failed", p.output)
            if not self.refreshed:
                p = self.bzr(["pull"], cwd=self.local, output=False)
                if p.returncode != 0:
                    raise VCSException("Bzr update failed", p.output)
                self.refreshed = True

        revargs = list(["-r", rev] if rev else [])
        p = self.bzr(["revert"] + revargs, cwd=self.local, output=False)
        if p.returncode != 0:
            raise VCSException("Bzr revert of '%s' failed" % rev, p.output)

    def _gettags(self):
        p = self.bzr(["tags"], cwd=self.local, output=False)
        return [tag.split("   ")[0].strip() for tag in p.output.splitlines()]

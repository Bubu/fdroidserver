# Bubu's experimental fdroidserver fork

This contains many usability improvements over the official fdroidserver tools. 
It's not ready for a release right now though, but feel free to browse through the recent commits.

Some notable changes:

* Automatically download/install the Android NDK (from [upstream!762](https://gitlab.com/fdroid/fdroidserver/-/merge_requests/762))
* Work with a fully prebuilt [vagrant buildserver box](https://app.vagrantup.com/bubu/boxes/fdroid-buildserver)
* Always sign with apksigner, so you always get apksig v3/v4
* Remove lots of legacy code (wiki updating, etc.)
* standardized formatting with `black`
* Remove localizations

Upstream changes will be integrated on an irregular basis.

# F-Droid Server

Server for [F-Droid](https://f-droid.org), the Free Software repository system
for Android.

The F-Droid server tools provide various scripts and tools that are
used to maintain the main
[F-Droid application repository](https://f-droid.org/packages).  You
can use these same tools to create your own additional or alternative
repository for publishing, or to assist in creating, testing and
submitting metadata to the main repository.

For documentation, please see <https://f-droid.org/docs/>, or you can
find the source for the documentation in
[fdroid/fdroid-website](https://gitlab.com/fdroid/fdroid-website).


### What is F-Droid?

F-Droid is an installable catalogue of FOSS (Free and Open Source Software)
applications for the Android platform. The client makes it easy to browse,
install, and keep track of updates on your device.


### Installing

There are many ways to install _fdroidserver_, they are documented on
the website:
https://f-droid.org/docs/Installing_the_Server_and_Repo_Tools

All sorts of other documentation lives there as well.


### Tests

There are many components to all of the tests for the components in
this git repo.  The most commonly used parts of well tested, while
some parts still lack tests.  This test suite has built over time a
bit haphazardly, so it is not as clean, organized, or complete as it
could be.  We welcome contributions.  Before rearchitecting any parts
of it, be sure to [contact us](https://f-droid.org/about) to discuss
the changes beforehand.


#### `fdroid` commands

The test suite for all of the `fdroid` commands is in the _tests/_
subdir.  _.gitlab-ci.yml_ runs this test suite on
various configurations.

* _tests/complete-ci-tests_ runs _pylint_ and all tests on two
  different pyvenvs
* _tests/run-tests_ runs the whole test suite
* _tests/*.TestCase_ are individual unit tests for all of the `fdroid`
  commands, which can be run separately, e.g. `./update.TestCase`.
